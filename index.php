<html>
<title>搜索引擎工具|安装</title>
<head>
    <link rel="stylesheet" href="static/css/app.css">
    <link rel="stylesheet" href="static/css/index.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body>
<h3>搜索引擎安装</h3>
<div class="container">
    <div class="from">
        <form>
            <div class="databse" id="database">
                <div class="form-group form-inline server">
                    <label class="col-sm-5 control-label">安装目录</label>
                    <input type="text" class="form-control " required name="directory" placeholder="搜素引擎安装目录">
                </div>
                <div class="form-group form-inline server">
                    <label class="col-sm-5 control-label">项目名称</label>
                    <input type="text" class="form-control" name="name" placeholder="搜索引擎项目名称">
                </div>
                <div class="form-group">
                    <input type="button" class="btn btn-success button" onclick="submitFrom()" value="开始安装">
                </div>
            </div>
        </form>
    </div>
    <div class="list" id="list">

    </div>
    <div class="next" id="next" style="display: block">
        <input type="button" class="btn btn-success next-button" onclick="nextWork()" value="下一步">
    </div>
    <div class="database" id="linkdatabase" style="display: none">
        <input type="button" class="btn btn-success next-button" onclick="linkdatabase()" value="连接数据库">
    </div>
</div>
<script src="static/js/jquery.js"></script>
<script src="static/js/app.js"></script>
<script src="static/js/index.js"></script>
</body>

</html>
