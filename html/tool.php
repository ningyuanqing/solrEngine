<html>
<title>搜索引擎工具|索引模版创建</title>
<head>
    <link rel="stylesheet" href="../static/css/app.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        body{
            background: #FFFFFF;
        }
        .databse{
            width: 300px;
            height: 330px;
            margin: 20px;
            background: #47908f;
        }
        .button{
            float: right;
        }
        #list ul{
            margin:0px;
            padding:0px;
            list-style-type:none;
            vertical-align:middle ;
            font-size: 20px;
        }
        #list li{
            float:left;
            display:block;

            width:25%;
            height:50px;
            line-height:20px;

            font-size:14px;
            font-weight:bold;
            color:#666666;

            text-decoration:none;
            text-align:left;

            background: #dcf1ff;
        }
        h3{
            text-align: center;
        }
    </style>
</head>

<body>
<h3>搜索引擎索引创建测试工具</h3>
<div class="container">
    <form>
        <div class="databse" id="database">
            <div class="form-group form-inline">
                <label class="col-sm-5 control-label">数据库链接</label>
                <input type="text" class="form-control " required name="host" placeholder="数据库连接host">
            </div>
            <div class="form-group form-inline">
                <label class="col-sm-5 control-label">数据库端口</label>
                <input type="text" class="form-control" min="0" required name="port" placeholder="数据库连接端口">
            </div>

            <div class="form-group form-inline">
                <label class="col-sm-5 control-label">数据库名称</label>
                <input type="text" class="form-control" name="database" placeholder="连接数据快名称">
            </div>

            <div class="form-group form-inline">
                <label class="col-sm-5 control-label">数据库用户</label>
                <input type="text" class="form-control" name="username" placeholder="数据库连接用户名称">
            </div>

            <div class="form-group form-inline">
                <label class="col-sm-5 control-label">数据库密码</label>
                <input type="password" class="form-control" name="password" placeholder="数据库连接用户密码">
            </div>
            <div class="form-group">
                <input type="button" class="btn btn-success button" onclick="submitFrom()" value="测试数据库连接">
            </div>
        </div>
    </form>
    <div id="list">
    </div>
</div>
<script src="../static/js/app.js"></script>
<script src="../static/js/jquery.js"></script>
<script type="text/javascript">
    function submitFrom() {
        var data = $('form').serialize()
        console.log(data)
        $.ajax({
            url:'../controller/database.php',
            data:data,
            success:function (res) {
                if(res.code == 200){
                    createTable(res.data);
                }
            }
        })
    }
    function createTable(data) {
        var count = data.length;
        console.log(count)
        $("#list").append("<div><td align='center' width='12%' >\n" +
            "        <input type=\"checkbox\" id=\"allChecks\" name=\"chb[]\" onclick=\"ckAll(this)\" /> 全选/全不选\n" +
            "    </td></div>")
        $("#list").append("<ul>");
        for (var i = 0; i < count; i++) {
            $("#list").append("<li ><input type='checkbox' name='chb[]' value="+ data[i]+">" + data[i] + "</checkbox></li>")
        }
        $("#list").append("<ul>");
        $("#list").append("<input type='button' class='btn btn-success button' onclick='next()' value='下一步'>")

    }

    function ckAll(obj){
        var chbs = document.getElementsByName("chb[]")
        if(obj.id){
            for (var i=1;i<chbs.length;i++){
                if(obj.checked == true){
                    var chb = chbs[i];
                    chb.checked = true;
                }
            }

            //全不选
            for (var i=1;i<chbs.length;i++){
                //若全选框的结果为没选中，则进行全不选操作，否则进入下一步
                if(obj.checked == false){
                    var chb = chbs[i];
                    chb.checked = false;
                }
            }
        }else{
            //若子选择全选，全选框也选中。
            if(chbs[1].checked && chbs[2].checked && chbs[3].checked && chbs[4].checked)
            {
                chbs[0].checked = true;
            }else//若子选项没有全选，全选框不选中。
            {
                chbs[0].checked = false;
            }
        }
    }

    function next() {
        var data = document.getElementsByName("chb[]");
        var database='';
        for (var i=0;i<data.length;i++){
            if(i>0){
                if(data[i].checked){
                    database +=data[i].value+"&&";
                }
            }
        }
        if(database == ''){
            alert("please choose one!!")
        }else{
            var base = new Base64();
            var result = base.encode(database);
            $.ajax({
                'url':'../controller/database.php',
                'data':{data:result,act:'action'},
                'type':'POST',
                success:function (res) {
                    console.log(res)
                }
            })
        }
    }

    function Base64() {

        // private property
        _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

        // public method for encoding
        this.encode = function (input) {
            var output = "";
            var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
            var i = 0;
            input = _utf8_encode(input);
            while (i < input.length) {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);
                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;
                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }
                output = output +
                    _keyStr.charAt(enc1) + _keyStr.charAt(enc2) +
                    _keyStr.charAt(enc3) + _keyStr.charAt(enc4);
            }
            return output;
        }

        // public method for decoding
        this.decode = function (input) {
            var output = "";
            var chr1, chr2, chr3;
            var enc1, enc2, enc3, enc4;
            var i = 0;
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
            while (i < input.length) {
                enc1 = _keyStr.indexOf(input.charAt(i++));
                enc2 = _keyStr.indexOf(input.charAt(i++));
                enc3 = _keyStr.indexOf(input.charAt(i++));
                enc4 = _keyStr.indexOf(input.charAt(i++));
                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;
                output = output + String.fromCharCode(chr1);
                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }
            }
            output = _utf8_decode(output);
            return output;
        }

        // private method for UTF-8 encoding
        _utf8_encode = function (string) {
            string = string.replace(/\r\n/g,"\n");
            var utftext = "";
            for (var n = 0; n < string.length; n++) {
                var c = string.charCodeAt(n);
                if (c < 128) {
                    utftext += String.fromCharCode(c);
                } else if((c > 127) && (c < 2048)) {
                    utftext += String.fromCharCode((c >> 6) | 192);
                    utftext += String.fromCharCode((c & 63) | 128);
                } else {
                    utftext += String.fromCharCode((c >> 12) | 224);
                    utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                    utftext += String.fromCharCode((c & 63) | 128);
                }

            }
            return utftext;
        }

        // private method for UTF-8 decoding
        _utf8_decode = function (utftext) {
            var string = "";
            var i = 0;
            var c = c1 = c2 = 0;
            while ( i < utftext.length ) {
                c = utftext.charCodeAt(i);
                if (c < 128) {
                    string += String.fromCharCode(c);
                    i++;
                } else if((c > 191) && (c < 224)) {
                    c2 = utftext.charCodeAt(i+1);
                    string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                    i += 2;
                } else {
                    c2 = utftext.charCodeAt(i+1);
                    c3 = utftext.charCodeAt(i+2);
                    string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                    i += 3;
                }
            }
            return string;
        }
    }
</script>
</body>

</html>
