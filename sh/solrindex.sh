#!/bin/bash

#搜索引擎安装路径
path="/usr/local/search"

#搜索引擎新建项目名称
project_name="engile"

#搜索引擎分词配置文件
managed_schema="$path/7.5.0/server/solr/$project_name/conf"

#安装好的初始化分词配置文件
solr_conf=$(cat $managed_schema/managed-schema)

index=$(cat ./solr_index.txt)

#判断是否已配置分词文件
if [[ $solr_conf == *$index* ]]
then
    result="包含"
else
    result="不包含"
fi

if [[ $result == "不包含" ]]
then
    gsed -i '128 r solr_index.txt' $managed_schema/managed-schema
    echo "开始写入数据库索引模版"
else
    echo "已存在"
fi
