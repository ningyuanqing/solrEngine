<?php
/**
 * Created by PhpStorm.
 * User: qmh
 * Date: 2019-09-20
 * Time: 11:18
 */

$contents = file_get_contents("../web.txt");
$encoding = mb_detect_encoding($contents,['GB2312','GBK','UTF-16','UCS-2','UTF-8','BIG5','ASCII']);
$file = fopen('../web.txt','r');
$text = [];
$i = 0;
while(!(feof($file))) {
    $str = '';
    $str = trim(fgets($file));
    if ($encoding != false) {
        $str = iconv($encoding, 'UTF-8', $str);
        if ($str != "" and $str != NULL) {
            $text[$i] = $str;
        }
    }
    else {
        $str = mb_convert_encoding ( $str, 'UTF-8','Unicode');
        $text[$i] = $str;
    }
    $i++;
}
$data = [
    'code'=>200,
    'data'=>$text
];
header('Content-Type: application/json');
$return = json_encode($data);
echo $return;


