<?php
/**
 * Created by PhpStorm.
 * User: qmh
 * Date: 2019-09-20
 * Time: 11:48
 */
$host     = $_POST['host'];
$port     = $_POST['port'];
$database = $_POST['database'];
$username = $_POST['username'];
$password = $_POST['password'];

$config = [
    'host'=>$host==null ? '192.168.1.10' : $host,
    'port'=>$port==null ? '3306' : $port,
    'dbname'=>$database==null ? 'engine' : $database,
    'user'=>$username==null ? 'root' : $username,
    'passwd'=>$password==null ? 'root' : $password
];

$hidden = [];
include_once "../db/DB.php";
$db = new DB($config);
$act = $_REQUEST['act'] ? $_REQUEST['act'] : 'table';
if($act == 'table'){
    $sql = "SELECT table_name FROM information_schema.TABLES where table_schema ='$config[dbname]'";
    $table = $db->doSql($sql);
    foreach ($table as $value){
        $arr[] = $value['table_name'];
    }
    header('Content-Type: application/json');
    $return = json_encode(['code'=>200,'data'=>$arr]);
    echo $return;exit;
}
elseif ($act == 'action') {
    $data = base64_decode($_REQUEST['data']);
    $database = array_filter(explode('&&', $data));
    //转化所传数据表名称为数组
    $database = json_decode(json_encode($database), true);
    $file = "./solr_index.txt";
    foreach ($database as $value) {
        if (!in_array($value, $hidden)) {
            $sql = "select column_name,column_comment,data_type from information_schema.columns where table_name='$value' and table_schema='$config[dbname]'";
            $columns = $db->doSql($sql);
            foreach ($columns as $val) {
                if ($val['data_type'] == 'varchar' || $val['data_type'] == 'char') {
                    $index_type = "text_ik";
                } elseif ($val['data_type'] == 'int') {
                    if (strlen($val['data_type']) == 10) {
                        $index_type = "string";
                    } else {
                        $index_type = 'pint';
                    }
                } elseif ($val['data_type'] == 'text') {
                    $index_type = "text_ik";
                } elseif ($val['data_type'] == 'pfloat') {
                    $index_type = 'float';
                } elseif ($val['data_type'] == 'timestamp') {
                    $index_type = 'string';
                } else {
                    $index_type = 'string';
                }
                $txt = '<field';
                $txt .= " name='$val[column_name]' type='$index_type'";
                $txt .= ' indexed="true" stored="true"';
                $txt .= '/>';
                $open = fopen($file, "a");
                fwrite($open, $txt . "\n");
            }
        }
    }
}



