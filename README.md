# 搜索引擎v2

#### 介绍
PHP脚本 shell脚本 对solr搜索引擎进行一键安装，一键配置分词以及数据库jar包，连接数据库创建搜索引擎字段模版

#### 软件架构
软件架构说明


#### 安装教程

1. 通过git clone https://gitee.com/qimh/solrEngine.git拉取项目到服务器项目根目录  
2. 必须在root账户行运行该项目，确保创建目录以及写入文件时有权限
3. 然后根据提示，点击安装即可。http://xxx.com/solrEngile

#### 使用说明

1. 搜索引擎默认安装目录为/usr/local/search
2. 默认项目名称为engile 在/usr/local/search/7.5.0/server/solr/目录下
3. 如若根据具体需要修改分词配置 请在/usr/local/search/7.5.0/server/solr/engile/conf 目录下修改
4. 基本功能已完成。还需细化、完善具体细节。

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
