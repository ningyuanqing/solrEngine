#!/bin/bash

project_name="engile"
url=${2}
echo ${project_name}

##解压路径
path="/usr/local/search/"
echo $path
##压缩包名字
name="solr.tar"

##从压缩里解压后的名字
aname="7.5.0"

if [ ! -e $path ]; then
echo "不存在"$path"路径，自动创建"
sudo mkdir $path
sleep 1
fi

##解压solr压缩包到指定路径下
time1=$(date)
echo ${time1}
tar -zxvf $name -C $path
echo "解压完毕"
sleep 2

##安装路径

apath= $path$aname

if [ -e $apath ]; then
echo "存在将要安装的路径，准备移除"
rm -rf $apath
sleep 2
echo "移除完毕"
fi

mkdir $apath

echo "安装完成"

echo "修改日志文件写入权限"

chmod -R 777 $path$aname'/server/logs'

echo "开始启动solr服务"

cd $path$aname

bin/solr restart -force

## solr服务启动成功  用solr命令生成要造的项目工程

echo "开始创建索引项目"

cd $path$aname

bin/solr create -c ${project_name} -force

echo "安装新建项目完成，开始写入配置文件"

exit 8

